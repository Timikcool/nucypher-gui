import React, { Component } from "react";
import { Route, Switch, NavLink, Redirect } from "react-router-dom";
import Dashboard from "./Dashboard";
import Wallets from "./Wallets";
import "./App.scss";
import Tokens from "./Tokens";
import Access from "./Access";
import Recieve from "./Recieve";
import Send from "./Send";
import Details from "./Details";
import Secrets from "./Secrets";
class App extends Component {
  constructor() {
    super();
    this.state = {
      wallets: [
        {
          id: "1",
          name: "First Name",
          amount: 25,
          adress:
            "6B86B273FF34FCE19D6B804EFF5A3F5747ADA4EAA22F1D49C01E52DDB7875B4B"
        },
        {
          id: "2",
          name: "Second Name",
          amount: 5,
          adress:
            "CB996A7D6DAD2D962A9A95B81E2730300F3706269FE274F9FCB68CDEB1CF8F05"
        }
      ]
    };
  }
  removeWallet(walletId) {
    const newWallets = this.state.wallets.filter(({ id }) => id !== walletId);
    this.setState({ wallets: newWallets });
  }
  addWallet() {
    let newWallet = {
      id: "newidfromserver",
      name: "newWallet",
      amount: 0,
      adress: "random"
    };
    this.setState({ wallets: [...this.state.wallets, newWallet] });
  }
  render() {
    return (
      <div className="main-container">
        <div className="nav-container">
          <NavLink
            to="/dashboard"
            activeClassName="nav-container__link__active"
          >
            <i className="fa fa-th-large" aria-hidden="true" /> <p>Dashboard</p>
          </NavLink>
          <NavLink to="/secrets" activeClassName="nav-container__link__active">
          <i className="fa fa-book" aria-hidden="true"></i><p>Secrets</p>
          </NavLink>
          <NavLink to="/wallets" activeClassName="nav-container__link__active">
            <i className="fa fa-credit-card" aria-hidden="true" />
            <p>Wallets</p>
          </NavLink>
          <NavLink to="/tokens" activeClassName="nav-container__link__active">
            <i className="fa fa-ethereum" aria-hidden="true" />
            <p>Tokens</p>
          </NavLink>
          
        </div>
        <div className="content-container">
          <Switch>
            <Redirect exact from="/" to="/dashboard" />
            <Route path="/dashboard" component={Dashboard} />
            <Route
              path="/wallets"
              render={props => (
                <Wallets
                  {...props}
                  wallets={this.state.wallets}
                  addWallet={this.addWallet.bind(this)}
                  removeWallet={this.removeWallet.bind(this)}
                />
              )}
            />
            <Route path="/secrets" component={Secrets} />
            <Route path="/tokens" component={Tokens} />
            <Route path="/access" component={Access} />
            <Route path="/send/:id" component={Send} />
            <Route path="/recieve/:id" component={Recieve} />
            <Route path="/details/:id" component={Details} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
