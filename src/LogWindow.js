import React, { Component } from 'react'

export default class LogsWindows extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state = { logs: this.props.messages }
	}
	componentWillReceiveProps(nextProps){
		console.log('nextProps', nextProps);
		
		this.setState({logs:nextProps.messages});
	}
	render() {
		console.log(this.state)
		let key = 0;
		return (
			<div className="log-container">{this.state.logs.map(e => <Log key={key++} message={e} />)}</div>
		)
	}
}
class Log extends Component {
	constructor(props) {
		super(props);
		this.props = props;
	}
	render() {
		console.log(this.props.message);
		return (
			<p key={this.props.key}>{this.props.message}</p>
		)
	}
}

