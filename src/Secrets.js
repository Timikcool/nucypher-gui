import React, { Component } from 'react'
import CreateSecret from './CreateSecret'
import { Route, Switch, NavLink, Redirect } from "react-router-dom";
import './Secrets.scss'
const Secrets = ({match}) =>
  {
    return (
        <React.Fragment>
      <div className="secrets-controls">
        <NavLink to={`${match.url}/create`} className="btn">Create new secret</NavLink>
        <NavLink to={`${match.url}/recover`} className="btn">Recover secret</NavLink>
      </div>
      <div className="secret-content">
        <Route path={`${match.url}/create`} component={CreateSecret}></Route>
      </div>
      </React.Fragment>
    )
  }
export default Secrets;