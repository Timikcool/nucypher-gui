import React, { Component } from "react";
import "./Wallets.scss";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
export default class Wallets extends Component {
  constructor(props) {
    super(props);

    this.state = {
      wallets: this.props.wallets
    };
  }
  render() {
    let key = 0;
    return (
      <React.Fragment>
        <div className="wallets-table">
          {this.state.wallets.map(e => (
            <Wallet
              key={key++}
              id={e.id}
              name={e.name}
              adress={e.adress}
              amount={e.amount}
            />
          ))}
        </div>
        <div className="wallets-controls">
          <div className="btn" onClick={this.props.addWallet}>
            Create Wallet
          </div>
          <div className="btn">Export Wallets</div>
        </div>
      </React.Fragment>
    );
  }
  componentWillReceiveProps({ wallets }) {
    this.setState({ wallets: wallets });
  }
}

class Wallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: props.id,
      amount: props.amount,
      adress: props.adress,
      name: props.name
    };
  }
  render() {
    return (
      <div className="wallet">
        <div>
          <p className="wallet-name">{this.state.name}</p>
          <p className="wallet-address">{this.state.adress}</p>
        </div>
        <div className="wallet-amount-container">
          <p className="wallet-amount">{this.state.amount}</p>
        </div>
        <div className="wallet-buttons-container">
          <Link to={`/send/${this.state.id}`} className="wallet-btn">
            Send
          </Link>
          <Link to={`/recieve/${this.state.id}`} className="wallet-btn">
            Recieve
          </Link>
          <Link to={`/details/${this.state.id}`} className="wallet-btn">
            Details
          </Link>
        </div>
      </div>
    );
  }
}
