import React, { Component } from 'react'
import Modal from 'react-modal';

const customStyles = {
    content : {
        width:'50%',
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)'
    }
  };

export default class CreateSecret extends Component {
constructor(props){
    super(props)
    this.state = {recoverNumber:1,totalNumber:1, secret:'', modalIsOpen:false};
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
}
openModal() {
    this.setState({modalIsOpen: true});
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  generateShares(){
    let sharesCount = this.state.totalNumber;
    let shares = []
    for (let i = 1; i <= sharesCount; i++) {
        shares.push({id:i});
    }
    return shares
  }
    render() {
    return (
        
      <div className="create-secret-container">
      <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
         <h2 ref={subtitle => this.subtitle = subtitle}>Hello</h2>
          <div>{this.generateShares().map((e)=><Share id={e.id} />)}</div>
          <button onClick={this.closeModal}>close</button>
        </Modal>
        <div className="enter-container">
            <h4>Enter secret here</h4>
            <div><textarea className="secret-input" /></div>
        </div>
        <div className="options-container">
            <h4>Options</h4>
            <div className="shares-to-recover">
                <input min="1" type="number" onChange={e => this.setState({recoverNumber:e.target.value})} />
                <p>Shares needed to recover the secret</p>
            </div>
            <div className="number-of-shares">
                <input min="1" type="number" onChange={e => this.setState({totalNumber:e.target.value})} />
                <p>Total number of shares</p>
            </div>
            <div className="result">{this.state.recoverNumber} of {this.state.totalNumber} shares needed to recover</div>
            <div className="btn" onClick={this.openModal}>Create Secret</div>
        </div>
      </div>
    )
  }
}
const Share = ({id}) => <div className="share"><span>Share #{id}</span><div className="btn modal-btn">Copy</div></div>