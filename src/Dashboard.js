import React, { Component } from "react";
import LogsWindows from "./LogWindow";
import "./Dashboard.scss";

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentDate: new Date(),
      currentNodeStatus: "offline",
      logs: ["Logs should be here message"]
    };
  }
  mockedLogs() {
    let intervalId = setInterval(() => {
      this.setState({ logs: [...this.state.logs, "Node is running"] });
    }, 5000);
    this.setState({ intervalId });
  }
  render() {
    return (
      <React.Fragment>
        <div className="connect-panel">
          <input className="url-search-input" type="url" />
          <button
            className="btn"
            onClick={e =>
              this.setState({ ...this.state, currentNodeStatus: "online" })
            }
          >
            Connect
          </button>
        </div>
        <div className="status-bar">
          Status: <span>{this.state.currentNodeStatus}</span>
        </div>
        <div className="control-panel">
          <button
            className="btn btn-run"
            disabled={this.state.currentNodeStatus === "running"}
            onClick={e => {
              this.setState({ ...this.state, currentNodeStatus: "running" });
              this.mockedLogs();
            }}
          >
            Run
          </button>
          <button
            className="btn btn-stop"
            disabled={this.state.currentNodeStatus === "stopped"}
            onClick={e => {
              clearInterval(this.state.intervalId);
              this.setState({ ...this.state, currentNodeStatus: "stopped" });
            }}
          >
            Stop
          </button>
          <button
            className="btn btn-reload"
            disabled={this.state.currentNodeStatus === "reloading"}
            onClick={e =>
              this.setState({ ...this.state, currentNodeStatus: "reloading" })
            }
          >
            Reload
          </button>
        </div>
        <LogsWindows messages={this.state.logs} />
      </React.Fragment>
    );
  }
}
