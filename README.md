# NuCypher GUI

### We developed a special library for this project: [nucypher-js](https://github.com/epexa/nucypher-js).

## Features
- Control NuCypher node
- Get log from node
- Permissions
- Create secret shards
- User info
- List wallets
- List tokens
- Send tokens
- Receive tokens

![_2018-12-01_15-59-58](https://user-images.githubusercontent.com/2198826/49328461-6ee73580-f582-11e8-8e7f-7ae59f10dab0.png)

![_2018-12-01_15-59-44](https://user-images.githubusercontent.com/2198826/49328462-6ee73580-f582-11e8-8b2f-0906b28979c1.png)

![_2018-12-01_15-59-32](https://user-images.githubusercontent.com/2198826/49328463-6ee73580-f582-11e8-87aa-f1595698f505.png)

![_2018-12-01_15-59-08](https://user-images.githubusercontent.com/2198826/49328464-6f7fcc00-f582-11e8-9991-8931df62f506.png)

![_2018-12-01_15-58-56](https://user-images.githubusercontent.com/2198826/49328465-6f7fcc00-f582-11e8-94bd-c214023517b2.png)

## This is a thin client that can be run:
- ### Standalone App:

#### On Linux: [dist/linux-unpacked.zip](dist/linux-unpacked.zip)

#### On Windows:

#### On MacOS:

- ### Chrome extension:
https://chrome.google.com/webstore/detail/eocinieajjcjaefgepmflbmcenfdkimi
- ### In browser: download [build](build) folder and run index.html
