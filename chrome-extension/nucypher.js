class NuCypher {
	
	constructor() {
		this.apiUrl = 'http://127.0.0.1:9000';
	}
	
	request(method, callback, params) {
		/* const query = {
			//id: 1,
			method: method,
			params: params
		};
		fetch(this.apiUrl, {
			method: 'POST',
			body: JSON.stringify(query)
		})
		.then((response) => { return response.json(); })
		.then((data) => {
			if ( ! data.error) {
				if ( ! data.params) data = data.result;
				callback(data);
			}
			else callback(null, data.error);
		}); */
		
		// TEMPORARY JSON STUB
		let response = {id: Math.floor((Math.random() * 1000000) + 1), method: method, params: params};
		response = JSON.stringify(response);
		callback(response);
	}

	grant(kfrags, callback) {
		this.request('grant', (res, err) => {
			callback(res, err);
		}, {
			kfrags: kfrags
		});
	}
	
	reencrypt(policy_id, callback) {
		this.request('reencrypt', (res, err) => {
			callback(res, err);
		}, {
			policy_id: policy_id
		});
	}
	
	revoke(policy_id, callback) {
		this.request('revoke', (res, err) => {
			callback(res, err);
		}, {
			policy_id: policy_id
		});
	}
	
	/* FUTURE METHOD */
	getWallets(policy_id, callback) {
		this.request('getWallets', (res, err) => {
			callback(res, err);
		}, {
			policy_id: policy_id
		});
	}
	
	/* FUTURE METHOD */
	createWallet(policy_id, callback) {
		this.request('createWallet', (res, err) => {
			callback(res, err);
		}, {
			policy_id: policy_id
		});
	}
	
	/* FUTURE METHOD */
	getTokens(policy_id, callback) {
		this.request('getTokens', (res, err) => {
			callback(res, err);
		}, {
			policy_id: policy_id
		});
	}
	
	/* FUTURE METHOD */
	createToken(policy_id, callback) {
		this.request('createToken', (res, err) => {
			callback(res, err);
		}, {
			policy_id: policy_id
		});
	}
	
	/* FUTURE METHOD */
	sendToken(policy_id, tokenName, amount, callback) {
		this.request('createToken', (res, err) => {
			callback(res, err);
		}, {
			policy_id: policy_id,
			tokenName: tokenName,
			amount: amount
		});
	}

}

const nucypher = new NuCypher();